**1\. Clone contoh project** 
``` bash
cd ~
git clone https://gitlab.com/ilosaurus/il-simple-counter.git
cd il-simple-counter
```

**2\. Buat Dockerfile & docker-compose.yaml**  

```Dockerfile
FROM composer:2 AS composer

FROM php:7-cli-alpine

ENV PHP_USER userbiasa
ENV PHP_GROUP groupbiasa
ENV PHP_UID 1000
ENV PHP_GID 1000

COPY --from=composer /usr/bin/composer /usr/bin/composer
RUN apk add --no-cache --virtual .build-deps $PHPIZE_DEPS \
  && apk add --no-cache tini \
  && pecl install redis \
  && docker-php-ext-enable redis \
  && addgroup -g ${PHP_GID} ${PHP_GROUP} \
  && adduser -D -u ${PHP_UID} -G ${PHP_GROUP} ${PHP_USER} \
  && docker-php-source delete \
  && apk del .build-deps && rm -rf /tmp/*

USER ${PHP_USER}
WORKDIR /app
EXPOSE 3000
ENTRYPOINT ["/app/app-entrypoint.sh"]
CMD ["php", "artisan", "key:generate"]
CMD ["php", "artisan", "serve", "--host=0.0.0.0", "--port=3000"]
```

```yaml
version: '3.5'
services:
  redis:
    image: redis:5-alpine
    ports:
      - 6379:6379
  myapp:
    tty: true
    image: laravel-simple-counter
    build: .
    depends_on:
      - redis
    environment:
      REDIS_HOST: redis
    ports:
      - 3000:3000
    volumes:
      - ./:/app

```

**3\. edit file .env ubah pada**
```bash
cp .env.example .env
```

```yaml
REDIS_HOST=127.0.0.1
menjadi
REDIS_HOST=redis
```


**4\. Jalankan aplikasi**  
```bash
docker-compose up -d
docker-compose ps
#docker-compose exec myapp php artisan key:generate
```

**5\. Test aplikasi**
```bash
curl localhost:3000
```

