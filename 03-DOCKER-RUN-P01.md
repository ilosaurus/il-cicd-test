# Docker Exercise 01 #

### Eksekusi di node 01 ###

**1. Check image redis**
```bash
docker search redis
```

**2. Menjalankan image redis**
```bash
docker run redis  # CTRL + c untuk keluar
docker run -d redis  # Berjalan di belakang layar (Background)
docker run -d --name redis1 redis # Memberi nama pada container
```

**3. Menampilkan container yang berjalan**. 
```bash
docker ps 
docker container ls
```

**4. Menampilkan semua container docker**. **[screenshot-2-2-4]**
```bash
docker ps -a
docker container ls -a
```

**5. Menampilkan deskripsi container <nama_container>**
```bash
docker inspect CONTAINER_NAME/CONTAINER_ID
docker inspect redis1
```

**6. Menampilkan isi logs dari container**. **[screenshot-2-2-6]**
```bash
docker logs CONTAINER_NAME/CONTAINER_ID
docker logs redis1
```

**7. Menampilkan live stream resource yang terpakai pada container**.   
```bash
docker stats CONTAINER_NAME/CONTAINER_ID
docker stats redis1
```

**8. Menampilkan running process pada container**.   
```bash
docker top CONTAINER_NAME/CONTAINER_ID
docker top redis1
```

**9. Mematikan container **
```bash
docker stop CONTAINER_NAME/CONTAINER_ID
docker stop redis1
```

**10. Hapus container**
```bash
docker rm redis1
docker rm -f redis1  #(secara paksa tanpa mematikan container)
```

# Docker Exercise 02   
**1. Check image nginx**
```bash
docker search nginx
```

**2. Menjalankan image nginx dan mengexpose port host**
```bash
docker run -d --name nginx1 -p 80:80 nginx:latest
```

**3. Menampilkan deskripsi container nginx**
```bash
docker inspect nginx1
```

**4. Menjalankan image nginx dan mendeklarasikan port container**
```bash
docker run -d --name nginx2 -p 80 nginx:latest
```

**5. uji browsing**. 
```bash
curl localhost:$(docker port nginx2 80| cut -d : -f 2)
```

**6. Menampilkan container (activate/exit)**
```bash
docker ps -a 
docker container ls -a
```

**7. Menampilkan docker image**
```bash
docker images
```

**8. Menghapus semua container**
```bash
docker rm -f $(docker ps -a -q)
```
