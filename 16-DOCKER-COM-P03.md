**1. Buat direktori my_app dan masuk ke direktori tersebut**
```bash
cd $HOME
mkdir latihan/my_app
cd latihan/my_app
```

**2. Buat file app.py**
```bash
vim app.py
...
import time

import redis
from flask import Flask


app = Flask(__name__)
cache = redis.Redis(host='redis', port=6379)


def get_hit_count():
    retries = 5
    while True:
        try:
            return cache.incr('hits')
        except redis.exceptions.ConnectionError as exc:
            if retries == 0:
                raise exc
            retries -= 1
            time.sleep(0.5)


@app.route('/')
def hello():
    count = get_hit_count()
    return 'Hello World! I have been seen {} times.\n'.format(count)

if __name__ == "__main__":
    app.run(host="0.0.0.0", debug=True)
...
```

**3. Buat file requirements.txt**
```bash
vim requirements.txt
...
flask
redis
...
```

**4. Buat file Dockerfile**
```bash
vim Dockerfile
...
FROM python:3.4-alpine
ADD . /code
WORKDIR /code
RUN pip install -r requirements.txt
CMD ["python", "app.py"]
...
```

**5. Buat file docker-compose.yml**
```bash
vim docker-compose.yml
...
version: '3.2'
services:
  web:
    build: .
    ports:
     - "5000:5000"
    volumes:
     - .:/code
  redis:
    image: "redis:alpine"
...
```

**6. Jalankan compose**
```bash
sudo docker-compose up -d
```

**7. Uji browsing**.  
```bash
curl http://localhost:5000
```

**8. Menampilkan compose aktif**.  
```bash
sudo docker-compose ps
```

**9. Menampilkan environment variable di service web**
```bash
sudo docker-compose run web env
```

**10. Hapus container, default network dan database app.py**
```bash
sudo docker-compose down --volumes
```
