# Docker Exercise 12  


**1. Jalankan container dari image nginx**
```bash
sudo docker run -itd --network host --name my_nginx nginx
```

**2. Uji browsing ke localhost**. 
```bash
curl http://localhost
```

**3. Verifikasi alamat IP dan bound port 80**. 
```bash
ip add
sudo netstat -tulpn | grep :80
```

**4. Cek alamat IP my_nginx**
```bash
docker inspect my_nginx |grep IPAddress
```

**5. Hapus container my_nginx**
```bash
sudo docker container rm -f my_nginx
```

____
# Docker Exercise 12 B  

**1. install nettools**  
```bash
apt install net-tools
```   

**2. Buat Docker Network macvlan**.   
```bash
docker network create -d macvlan --subnet=10.55.55.0/24 --gateway=10.55.55.1  -o parent=ens3 pub_net
docker network ls
```  

**3. Indentifikasi Docker Network macvlan**.   
```bash
docker network inspect pub_net
```


**4. Testing Container dengan Docker Network macvlan**.   

```bash
docker  run --net=pub_net --ip=10.55.55.41 -itd alpine /bin/sh
```


**5. Buat Virtual Network Namespace**.   
```bash
ip link add mac0 link ens3 type macvlan mode bridge
ip addr add 10.55.55.31/24 dev mac0
ifconfig mac0 up
```

**6. Uji Coba Container dengan Docker Network macvlan**.   
```bash
docker run --net=pub_net -d --ip=10.55.55.42  nginx
docker run --net=pub_net -d --ip=10.55.55.43  nginx
```
