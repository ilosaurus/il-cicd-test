# Docker Exercise 19


**1\. Install Docker & Docker Compose di Node02**  

**2\. Buat direktori docker registry**  
```bash
apt install apache2-utils  -y
mkdir -p /data/registry
cd /data/registry
```

**3\. Buat Docker Compose**  
```bash
vim docker-compose.yml
```



```yaml
version: '2.0'
services:
  registry:
    image: registry:2.7
    ports:
      - 5000:5000
    volumes:
      - ./registry-data:/var/lib/registry
    networks:
      - registry-ui-net

  ui:
    image: joxit/docker-registry-ui:1.5-static
    ports:
      - 80:80
    environment:
      - REGISTRY_TITLE=My Private Docker Registry
      - REGISTRY_URL=http://registry:5000
    depends_on:
      - registry
    networks:
      - registry-ui-net

networks:
  registry-ui-net:
```

```bash
docker-compose up -d
```
**4\. Verifikasi registry**  
```bash
curl  localhost:5000/v2/_catalog
```


**5\. Testing push image**  
```bash
vim /etc/docker/daemon.json
```
```json
{
    "insecure-registries" : [ "10.60.60.102:5000" ]
}
```
```bash
systemctl restart docker
systemctl status docker
docker images

```

```bash
docker tag nginx:latest localhost:5000/test-image-nginx
docker push localhost:5000/test-image-nginx
curl  localhost:5000/v2/_catalog
```
