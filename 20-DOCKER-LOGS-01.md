### Logging
#### Docker image history

**1. Check pada image docker**
```bash
docker images
docker image ls 
```

**2. Pilih salah satu untuk di check**. 
```bash
docker history redis:latest
```

#### Filesystem and Log Inspection 
**1. Jalankan nginx**
```bash
docker run -dit -p 80:80 --name nginx1 nginx
```

**2. Check Filesystem pada nginx**. 
```bash
docker diff nginx1
```

**3. Cek Log**
```bash
docker logs --details nginx1
docker logs --timestamps nginx1
docker logs nginx1
```
