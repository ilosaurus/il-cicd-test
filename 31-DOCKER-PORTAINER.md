**1\. Download file compose Portainer**  
```bash
cd ../
mkdir latihan-portainer
cd latihan-portainer
curl -L https://downloads.portainer.io/portainer-agent-stack.yml -o portainer-agent-stack.yml
```

**2\. Menjalankan stack portainer**
```bash
sudo docker stack deploy --compose-file=portainer-agent-stack.yml portainer
sudo docker stack ls
sudo docker stack services portainer
```  
  
**3\. Mengakses web portainer**  
- Akses dengan web browser dan masukkan alamat ip atau dns manager node dengan port 9000

**4\. Menjalankan web app baru dengan portainer**  
- pada menu navigasi bagian kiri portainer, pilih App Templates  
- pilih template Wordpress
- isikan nama stack dan password root database  
- klik Deploy the stack

