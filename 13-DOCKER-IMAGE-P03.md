# Docker Exercise 15


**1. Install Unzip**
```bash
apt install unzip
```

**2. Clone Repositori CI4**
```bash
git clone https://gitlab.com/ilosaurus/il-ci4-docer.git
```

**3. Buat Dockerfile**

```bash
vim Dockerfile
```

**4. Build Image**
```bash
docker build -t ci4 .
```

**5. Running Container CI4**
```bash
docker run --name=ci4 -d -p 8989:80 ci4:latest
```

**6. Testing Akses ke container**
```bash
curl localhost:8989
```
