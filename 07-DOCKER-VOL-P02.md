
# Docker Exercise 09 

**1. Membuat Direktori di node01**
```bash
mkdir -p /data/nfs-storage01/
```

**2. Create the NFS Server with docker**

```bash
docker run -itd --privileged \
  --restart unless-stopped \
  -e SHARED_DIRECTORY=/data \
  -v /data/nfs-storage01:/data \
  -p 2049:2049 \
  itsthenetwork/nfs-server-alpine:12
```

**3. Mounting volume di client nfs**


## Mounting NFS Server di node02

```bash

sudo apt update
sudo apt install nfs-client -y
sudo mount -v -t nfs4 il-node01:/ /mnt
df -h

touch /mnt/file1.txt
touch /mnt/file2.txt
exit
```

**4. Verifikasi di node01**. 
```bash
ls /data/nfs-storage01/
```

**5. Lepaskan montingan nfs di node02**  
```bash
umount /mnt
```

**6. Hapus semua container**  
```bash
docker rm -f $(docker ps -a -q)
```
