# Gitlab Runner   

**1. Install GitLab Runner.**  
```bash
sudo curl -L --output /usr/local/bin/gitlab-runner "https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64"
```

**2. Change permission GitLab Runner.**  
```bash
sudo chmod +x /usr/local/bin/gitlab-runner
```

**3. Create a GitLab CI user.** 
```bash
sudo useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash
```

**4. Install and run GitLab Runner service.** 
```bash
sudo gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner

```

**5. Set Permission and Verify the GitLab Runner.** 
```bash
echo 'gitlab-runner ALL=(ALL) NOPASSWD: ALL' >  /etc/sudoers.d/gitlab
sudo gitlab-runner start
sudo gitlab-runner status
sudo gitlab-runner verify
```
