**1. membuat working directory**
```bash
cd $HOME
mkdir -p latihan/latihan01-volume
cd latihan/latihan01-volume
```

**2. membuat data volume**
```bash
for i in {1..10};do touch file-$i;done
sudo docker create -v /test-volume --name test-volume busybox
sudo docker cp . test-volume:/test-volume
```
**3. mount volumes**. 
```bash
sudo docker run --volumes-from test-volume ubuntu ls /test-volume
```

**4. Hapus semua container**
```bash
docker rm -f $(docker ps -a -q)
```
