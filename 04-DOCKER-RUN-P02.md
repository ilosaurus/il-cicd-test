# Docker Exercise 03 #

**1. Check image nginx**
```bash
docker search nginx
```

**2. Menjalankan image nginx dan mengexpose port**
```bash
docker run -d --name nginx1 -p 8080:80 nginx:latest
```

**3. Menampilkan deskripsi container nginx**
```bash
docker inspect nginx1
```

**4. Menjalankan image nginx dan mengexpose port**
```bash
docker run -d --name nginx2 -p 8081:80 nginx:latest
```

**5. Menampilkan container (activate/exit)**. 
```bash
docker ps -a 
docker container ls -a
```

**6. Check access pada container**. 
```bash
curl localhost:8080
curl localhost:8081
```

**7. Masuk ke dalam container**
```bash
docker exec -it nginx2 /bin/bash
```

**8. Update dan install editor pada container**
```bash
apt-get update -y && apt-get install nano -y
```

**9. Edit pada index.html dan ubah pada welcome to nginx**
```bash
nano index.html
...
hello, testing.
...

mv index.html /usr/share/nginx/html
```

**10. Restart service nginx**
```bash
service nginx restart
```

**11. Menjalankan ulang container**
```bash
docker start nginx2
```

**12. Menampilkan container**
```bash
docker ps
docker container ls
```

**13. Check access pada container**.  
```bash
curl localhost:8080
curl localhost:8081
```

**14. Menampilkan deskripsi container <nama_container>**
```bash
docker inspect nginx1
docker inspect nginx2
```

**15. Menampilkan isi logs dari container**
```bash
docker logs nginx1
docker logs nginx2
```

**16. Menampilkan live stream resource yang terpakai pada container**
```bash
docker stats nginx1
docker stats nginx2
```

**17. Menampilkan running process pada container**
```bash
docker top nginx1
docker top nginx2
```

**18. Menghapus semua container**
```bash
docker rm -f $(docker ps -a -q)
```

_____

# Docker Exercise 04 #

**1. Check image ubuntu**
```bash
docker search ubuntu
```

**2. Mengambil image ubuntu**
```bash
docker pull ubuntu
```

**3. Menjalankan container dan langsung menuju consolenya**
```bash
docker run -it --name ubuntu1 ubuntu

keluar dari container dengan Ctrl+d atau exit 

docker ps -a
```

**4. Menjalankan container dan langsung dihapus**
```bash
docker run -it --rm --name ubuntu2 ubuntu

keluar dari container dengan Ctrl+d atau exit 

docker ps -a
```

**5. Menghapus semua container**
```bash
docker rm -f $(docker ps -a -q)
```
