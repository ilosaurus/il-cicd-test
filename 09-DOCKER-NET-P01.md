# Docker Exercise 11   


**1. Tampilkan daftar network saat ini**
```bash
sudo docker network ls
```

**2. Jalankan 2 container alpine yang menjalankan shell ash.**
```bash
docker run -dit --name alpine1 alpine ash
docker run -dit --name alpine2 alpine ash
docker container ls
```

**3. Buat network baru dan tambahkan ke container**
```bash
docker network create --driver bridge bridge1
docker network connect bridge1 alpine1
```

**4. cek ip container alpine2**
```bash
docker inspect alpine2
```

**5. Masuk ke container alpine1**
```bash
docker exec -it alpine1 sh
```

**6. Tampilkan alamat IP container alpine1**. 
```bash
ip add
```

**7. Uji ping ke internet (sukses)**. 
```bash
ping -c 3 8.8.8.8
```

**8. Uji ping ke alamat IP container alpine2 (sukses)**. 
```bash
ping -c 3 172.17.YYY.YYY
```

**9. Uji ping ke nama container alpine2 (gagal)**.
```bash
ping -c 3 alpine2
```

**10. Keluar dari container alpine1 tanpa menutup shell tekan Ctrl+P, Ctrl+Q**

**11. Hapus kedua container**
```bash
sudo docker container rm -f alpine1 alpine2
```
