## Health Check latihan01

**1. Buat direktori**
```bash
cd $HOME
mkdir hc-latihan01
cd hc-latihan01
```

**2. Buat file Dockerfile**
```bash
vim Dockerfile
...
FROM katacoda/docker-http-server:health
HEALTHCHECK --interval=1s --retries=3 \
	CMD curl --fail http://localhost:80/ || exit 1
...
```

**3. Buat image**
```bash
docker build -t http-healthcheck .
```

**4. Jalankan image**
```bash
docker run -d -p 80:80 --name http-healthcheck http-healthcheck
```

**5. Check image**. 
```bash
docker ps
# check pada bagian status
```

**6. check dengan curl**. 
```bash
  curl http://localhost/
# maka dapat di akses
```

**7. Check untuk di unhealthy**. 
```bash
curl http://localhost/unhealthy
```

**8. Check pada docker container status**
```bash
docker container ls
```

**9. Check dengan curl**. 
```bash
curl http://localhost/
```

## Health Check latihan02

**1. Buat direktori baru**
```bash
cd $HOME
mkdir hc-latihan02
cd hc-latihan02
```

**2. Buat file server.js**
```bash
vim server.js
...
"use strict";
const http = require('http');

function createServer () {
	return http.createServer(function (req, res) {
		res.writeHead(200, {'Content-Type': 'text/plain'});
		res.end('OK\n');
	}).listen(8080);
}

let server = createServer();

http.createServer(function (req, res) {
	res.writeHead(200, {'Content-Type': 'text/plain'});
	if (server) {
		server.close();
		server = null;
		res.end('Shutting down...\n');
	} else {
		server = createServer();
		res.end('Starting up...\n');
	}
}).listen(8081);
...
```

**3. Buat file Dockerfile**
```bash
vim Dockerfile
...
FROM node
COPY server.js /
EXPOSE 8080 8081
HEALTHCHECK --interval=5s --timeout=10s --retries=3 CMD curl -sS 127.0.0.1:8080 || exit 1
CMD ["node","server.js"]
...
```

**5. Buat image**
```bash
docker build -t node-server .
```

**6. Jalankan image**
```bash
docker run -d --name nodeserver -p 8080:8080 -p 8081:8081 node-server
```

**7. Check container**. 
```bash
curl 127.0.0.1:8080
docker ps 
docker inspect --format "{{ json .State.Health }}" nodeserver 
```

**8. Check container**. 
```bash
curl 127.0.0.1:8081
docker ps 
docker inspect --format "{{ json .State.Health }}" nodeserver 
```

**9. Check Container**
```bash
curl 127.0.0.1:8081
docker ps
docker inspect --format "{{ json .State.Health }}" nodeserver
```
