### **Instruction**  

1\. If there is variable **X** , change it with your presence number.

2\. If there is **[username]**, change it with **your username**.

3\. VM specifications.  
	- **podX-node01** : Ubuntu 20.04 (2vcpu, 4gb ram, 20gb disk)    
	- **podX-node02** : Ubuntu 20.04 (2vcpu, 4gb ram, 20gb disk)  

4\. Make sure IP, Gateway, DNS Resolver, & Hostname are correct.

**Node podX-node01**

- IP Address : 10.10.X.101/24
- Gateway : 10.10.X.1
- Hostname : podX-node01

**Node podX-node02**

- IP Address : 10.10.X.102/24
- Gateway : 10.10.X.1
- Hostname : podX-node02


5\. Edit name resolution
```bash
sudo vim /etc/hosts
```

```bash
.....
10.10.X.10 podX-node01
10.10.X.20 podX-node02
.....
```

6\. Verify connectivity.  **[screenshot-1-1-6]**
```bash
ping -c 3 podX-node01
ping -c 3 podX-node02
```
