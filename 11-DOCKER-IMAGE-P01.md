# Docker Exercise 13

**1. Clone repository Node**
```bash
git clone https://github.com/spkane/docker-node-hello.git  --config core.autocrlf=input latihan01
```
    
**2. Masuk ke dalam direktori**
```bash
cd latihan01
```

**3. Buat image**
```bash
docker build -t node-latihan01 .
```

**4. Buat container**
```bash
docker run -d --rm --name node-latihan01 -p 8080:8080 node-latihan01
```

**5. Coba akses port**. 
```bash
curl localhost:8080
```

**6. Hapus image dan container **
```bash
docker stop node-latihan01
docker rm node-latihan01
docker rmi node-latihan01
```
____

# Docker Exercise 14

**1. Buat direktori latihan02**
```bash
cd $HOME
mkdir latihan02
cd latihan02
```

**2. Buat file Dockerfile**. 
```bash
vim Dockerfile
...
# Use whalesay image as a base image
FROM docker/whalesay:latest

# Install fortunes
RUN apt -y update && apt install -y fortunes

# Execute command
CMD /usr/games/fortune -a | cowsay
...
```

**3. Bangun image dari Dockerfile**
```bash
docker build -t docker-whale .
```

**4. Tampilkan image yang sudah dibangun**. 
```bash
docker image ls
```

**5. Uji jalankan image**
```bash
docker run docker-whale
```

**6. Menampilkan container**. 
```bash
docker ps
docker container ls -a
```

**7. Hapus semua container**
