# Docker Exercise 14

**1. Buat direktori latihan03**
```bash
cd $HOME
mkdir latihan03
cd latihan03
```

**2. Clone repository**
```bash
git clone https://github.com/rivawahyuda/mywebsite.git
cd mywebsite
```

**3. Buat Dockerfile**. 
```bash
vim Dockerfile
...
FROM ubuntu:latest
RUN apt update -y && apt install -y tzdata && apt install -y apache2
ADD . /var/www/html/
EXPOSE 80
CMD ["/usr/sbin/apache2ctl", "-D", "FOREGROUND"]
...
```

**4. Build image**
```bash
sudo docker build -t myweb .
```

**#5. Run image**
```bash
sudo docker run -d -p 4000:80 myweb 
```

**6. Uji browsing**. 
```bash
ssh root@labZ.btech.id -DYYYY

Akses di browser 10.X.X.10:4000
```

**7. Tampilkan container Up**. 
```bash
sudo docker container ls -a
```

**8. Stop container**
```bash
sudo docker container stop [CONTAINER ID]
```  
____
  
# Docker Exercise 15

**1. Jika belum punya Docker ID, register di [https://id.docker.com](https://id.docker.com)**

**2. Login dengan Docker ID**

```bash
sudo docker login
```

**3. Buat direktori latihan04**
```bash
cd $HOME
mkdir latihan04
cd latihan04
```

**4. Membuat file flask**.  
```bash
vim app.py
...
from flask import Flask
app = Flask(__name__)

@app.route('/')
def hello_world():
    return 'Hey, we have Flask in a Docker container!'


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
...
```

**5. Membuat requirements.txt**.  
```bash
echo "Flask==0.10.1" >> requirements.txt
```

**6. Membuat file Dockerfilenya**.  
```bash
vim Dockerfile
...
FROM ubuntu:16.04
RUN mkdir /app
RUN apt-get update -y && \
		apt-get install python-pip python-dev -y
		
COPY ./requirements.txt /app
COPY . /app

WORKDIR /app
RUN pip install -r requirements.txt

ENTRYPOINT ["python"]
CMD ["app.py"]
...
```

**7. Buat image dari Dockerfile**
```bash
docker build -t flask-latihan04 .
```

**8. Tag image**
```bash
sudo docker tag flask-latihan04 [username]/flask-latihan04:latest
```

**9. Push image**
```bash
sudo docker push [username]/flask-latihan04:latest
```

**10. Menjalankan image **
```bash
sudo docker run -d -p 5000:5000 --name flask04 [username]/flask-latihan04
```

**11 Uji browsing**.  
```bash
curl localhost:5000
```
