#### **Installing Docker **  
As already mentioned, this is the most straightforward installation out of the three systems we will be looking at. To install Docker, simply run the following command from a Terminal session :   

**1. Instal Docker**
```bash
sudo apt -y update 
sudo apt -y install apt-transport-https ca-certificates curl gnupg-agent software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo apt-key fingerprint 0EBFCD88
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
sudo apt -y update
sudo apt -y install docker-ce docker-ce-cli containerd.io
sudo systemctl status docker
```

**2. Menampilkan versi docker**. 
```bash
sudo docker version
```

**3. Menampilkan detil instalasi docker**
```bash
sudo docker info
```

**4. Uji instalasi docker**
```bash
sudo docker run hello-world
```

**5. Menampilkan image yang sudah di-download**. 
```bash
sudo docker image ls
```

**6. Menampilkan semua container (active ataupun exit)**
```bash
sudo docker container ls -a
```

**7. Menambahkan user ke dalam group docker**
```bash
sudo groupadd docker
sudo usermod -aG docker $USER
```
  
**8. test docker tanpa sudo**. 
```bash
docker container ls
```
