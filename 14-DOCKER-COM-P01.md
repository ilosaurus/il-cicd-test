#### Introduction

Before we look at using Docker Compose more, we should have a deeper dive into docker-compose.yml files as these are the heart of Docker Compose.

> YAML is a recursive acronym that stands for YAML Ain't Markup Language. It is used by a lot of different applications for both configuration and also for defining data in a human-readable structured data format. The indentation you see in the examples is very important as it helps to define the structure of the data.

# Docker Exercise 17

**1. Unduh Compose**
```bash
sudo apt install -y docker-compose
```

**2. Uji instalasi**. 
```bash
sudo docker-compose --version
```
____

# Docker Exercise 18


**1. Buat direktori my_wordpress dan masuk ke direktori tersebut**
```bash
cd $HOME
mkdir -p latihan/my_wordpress
cd latihan/my_wordpress
```

**2. Buat file docker-compose.yml**
```bash
vim docker-compose.yml
```
```yaml
version: '3.2'

services:
   db:
     image: mysql:5.7
     volumes:
       - dbdata:/var/lib/mysql
     restart: always
     environment:
       MYSQL_ROOT_PASSWORD: somewordpress
       MYSQL_DATABASE: wordpress
       MYSQL_USER: wordpress
       MYSQL_PASSWORD: wordpress

   wordpress:
     depends_on:
       - db
     image: wordpress:latest
     ports:
       - "8000:80"
     restart: always
     environment:
       WORDPRESS_DB_HOST: db:3306
       WORDPRESS_DB_USER: wordpress
       WORDPRESS_DB_PASSWORD: wordpress
volumes:
    dbdata:
```

**3. Jalankan compose**
```bash
sudo docker-compose up -d
```

**4. Tampilkan daftar container**. 
```bash
sudo docker container ls
```

**5. Uji browsing ke ke halaman wordpress yang sudah dibuat**.  

**6. Hapus container, default network dan database wordpress**
```bash
sudo docker-compose down --volumes
```
