#### **Creating a Project on GitLab** 
    

**1. Clone the Project.**
```bash
cd ~/
git clone https://gitlab.com/ilosaurus/ilosaurus-docker
ilosaurus-docker
```
**2. Create Gitlab CI Yaml.**

```yaml
stages:
- deploy

docker:
  stage: deploy
  script:
    - docker-compose down --rmi all
    - docker-compose up -d --build
  only:
    - master
  tags:
    - apps
................................................................
```

**3. Get Token Runner.** 
```bash
# Use your browser then access to : https://gitlab.com/ilosaurus/ilosaurus-docker
# Go to Settings > CI / CD > Runners > Shared Runners. Click Disable shared Runners.
# Go to Setting > CI / CD > Runners > Setup a specific Runner manually. Copy the registration token.
```


**4. Register GitLab Runner Executor Shell. (Change the REGISTRATION_TOKEN with Token Runner. )**  

```bash
sudo gitlab-runner register -n \
  --url http://gitlab.com/ \
  --registration-token [REGISTRATION_TOKEN] \
  --executor shell \
  --description "apps" \
  --tag-list "apps"
```



