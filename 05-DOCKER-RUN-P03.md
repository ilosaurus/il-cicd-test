# Docker Exercise 05 

### Adding network links ###

**1. Menjalankan docker mysql**
```bash
docker run -d --name my-own-mysql -e MYSQL_ROOT_PASSWORD=RAHASIA -e MYSQL_DATABASE=latihan01 mysql
```

**2. Pull image phpmyadmin**
```bash
docker pull phpmyadmin/phpmyadmin:latest
```

**3. Menjalankan docker phpmyadmin dan hubungkan dengan mysql**
```bash
docker run --name my-own-phpmyadmin -d --link my-own-mysql:db -p 8090:80 phpmyadmin/phpmyadmin
```

**4. Uji browsing**.  
```bash
buka dibrowser http://10.X.X.10:8090 kemudian login dengan user: `root` dan password: `RAHASIA` 
```
**5. Menghapus semua container**
```bash
docker rm -f $(docker ps -a -q)
```

___

# Docker Exercise 06 

**1. Jalankan container ubuntu**
```bash
docker run -dit --name ubuntu1 ubuntu
```

***2. Menampilkan daftar container**
```bash
docker ps
```

**3. Apabila container ubuntu1 ada, maka pause container tersebut**
```bash
docker pause ubuntu1
```

**4. Check pada daftar container**. 
```bash
docker ps
```

**5. Check pada resource**
```bash
docker stats ubuntu1
```

**6. Unpause container ubuntu1**
```bash
docker unpause ubuntu1
```

**7. Menghapus semua container**
```bash
docker rm -f $(docker ps -a -q)
```

# docker run (latihan07) #
### Adding spesification container (RAM and CPU) ###

**1. Membuat Datase Container**
```bash
docker container run -d --name ch6_mariadb --memory 256m --cpu-shares 1024 --cap-drop net_raw -e MYSQL_ROOT_PASSWORD=test mariadb:5.5
```

**2. Membuat container wordpress**
```bash
docker container run -d -p 80:80 -P --name ch6_wordpress  --memory 512m --cpu-shares 512 \
--cap-drop net_raw --link ch6_mariadb:mysql -e WORDPRESS_DB_PASSWORD=test wordpress:5.0.0-php7.2-apache
```

**3. Check logs, running process, and resource**.  
```bash
docker logs ch6_mariadb
docker logs ch6_wordpress

docker top ch6_mariadb
docker top ch6_wordpress

docker stats ch6_mariadb
docker stats ch6_wordpress
```

**4. Uji browsing**.   

```bash
buka di browser http://10.X.X.10 kemudian selesaikan tahap instalasinya
```

**5. Menghapus semua container**
```bash
docker rm -f $(docker ps -a -q)
```
