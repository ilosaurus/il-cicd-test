# Docker Exercise 10   

**1. Membuat volume**  
```bash
docker volume create my-vol
```

**2. Menampilkan daftar volume**.  
```bash
docker volume ls
```

**3. Menampilkan detil volume**
```bash
docker volume inspect my-vol
```

**4. Jalankan container dengan volume**
```bash
docker run -d --name=nginx-test -v my-vol:/usr/share/nginx/html -p 4005:80 nginx:latest
```

**5. Tampilkan alamat IP container**
```bash
docker inspect nginx-test | grep -i ipaddress
```

**6. Uji browsing ke alamat IP container**.   
```bash
curl http://172.17.XXX.XXX
```

**7. Buat file index.html dan pindahkan ke direktori source volume**
```bash
sudo echo "This is from my-vol source directory" > index.html
sudo mv index.html /var/lib/docker/volumes/my-vol/_data
```

**8. Uji browsing ke alamat IP container**.  
```bash
curl http://172.17.XXX.XXX
```

**9. Jalankan container dengan read-only volume**
```bash
docker run -d --name=nginxtest-rovol -v my-vol:/usr/share/nginx/html:ro nginx:latest
```

**10. Tampilkan detil container nginxtest-rovol dan perhatikan Mode di section Mounts**
```bash
docker inspect nginxtest-rovol
```

**11. Hapus semua container**
```bash
docker rm -f $(docker ps -a -q)
```
