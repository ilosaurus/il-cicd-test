**1\.** Buat Docker Compose Untuk aplikasi dari ci4 dengan nama service app  
**2\.** Expose ke port 7878  
**3\.** include service db Image mysql  

___


```yaml
version: '3'
services:
    db:
        image: mysql
        environment: 
            MYSQL_DATABASE: root
            MYSQL_ROOT_PASSWORD: toor
        command: --default-authentication-plugin=mysql_native_password
        restart: unless-stopped
        volumes:
            - ./db_data:/usr/data
        ports:
            - 3310:3306
    web:
        build:
            context: .
            dockerfile: Dockerfile
        restart: unless-stopped
        ports:
            - 8282:80
        depends_on: 
            - db
        links:
            - db
    adminer:
        image: adminer
        restart: unless-stopped
        ports:
            - 8082:8080
volumes:
    db_data:
    src:
```
